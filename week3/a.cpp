#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>
#include <map>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010

int t, r, c, temp;
int dp[210][210];

int tot(int i, int j, int ii, int jj) {
	int res = dp[ii][jj] - dp[ii][j - 1] - dp[i - 1][jj] + dp[i - 1][j - 1];
	return res;
}

int brute () {
	int ans = 0;
	
	FOR(i, 1, r) FOR(j, 1, c) FOR(ii, i, r) FOR(jj, j, c) {
		
		if (tot(i, j, ii, jj) % 2 == 0) {
			ans++;
		}
	}
	return ans;
}	

int main () {
	cin >> t;
	
	while (t--) {
		CLEAR(dp, 0);
		
		cin >> r >> c;
		FOR(i, 1, r) FOR(j, 1, c) {
			cin >> temp;
			dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + temp;
		}
		
		cout << brute() << endl;
	}
}





