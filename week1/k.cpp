#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define Sec second
#define CLEAR(x, val) memset(x, val, sizeof(x))

int T, N, S, x, y;
pii kor[110];
vi adj[110];
bool flag[110];


bool has_intersection(int i, int j) {
	int x1, x2, y1, y2, p1, p2, q1, q2;
	
	x1 = kor[i].F;
	x2 = x1 + S;
	y1 = kor[i].Sec;
	y2 = y1 + S;
	
	p1 = kor[j].F;
	p2 = p1 + S;
	q1 = kor[j].Sec;
	q2 = q1 + S;
	
	bool a = x1 >= p2;
	bool b = x2 <= p1;
	bool c = y1 >= q2;
	bool d = y2 <= q1;
	
	bool non_overlap = a | b | c | d;
	
	return !non_overlap;	
}

int bfs(int root) {
	queue < int > q;
	q.push(root);
	
	flag[root] = 1;

	int u, v, len, ans = 1;
	while (!q.empty()) {
		u = q.front(); q.pop();
		
		len = adj[u].size();
		REP(i, len) {
			v = adj[u][i];
			
			if (!flag[v]) {
				q.push(v);
				flag[v] = 1;
				ans++;
			}
		}
	}
	
	return ans;
}

int main () {
	cin >> T;
	
	while (T--) {
		cin >> N >> S;
		
		REP(i, N) {
			adj[i].clear();
			scanf("%d %d", &x, &y);
			kor[i] = mp(x, y);
		}
		
		REP(i, N) {
			FOR(j, i + 1, N - 1) {
				if (has_intersection(i, j)) {
					adj[i].pb(j);
					adj[j].pb(i);
				}
			}
		}
		
		CLEAR(flag, 0);
		
		int temp, ans = 1;
		REP(i, N) {
			if (!flag[i]) {
				temp = bfs(i);
				if (temp > ans) ans = temp;
			}
		}
		
		cout << ans << endl;
	}
}



