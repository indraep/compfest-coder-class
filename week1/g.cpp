#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>
#include <map>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010

int tc, N;

pair < int, string > memo[1010];

string dp(int n) {
	if (n == 0) return "";
	if (memo[n].F != -1) return memo[n].S;
	
	string ans = dp(n - 1) + 'X';
	string temp;
	
	if (n % 2 == 0) {
		temp = dp(n / 2) + 'Y';
		if (ans.length() > temp.length())
			ans = temp;
	}
	memo[n] = mp(ans.length(), ans);
	return ans;
}

int main () {
	REP(i, 1005) memo[i].F = -1;
	cin >> tc;
	
	while (tc--) {
		cin >> N;
		cout << dp(N) << endl;
	}
}







