#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010

int T, N;

bool puter(int n) {
	if (n % 7 == 0) return 1;
	
	while (n > 0) {
		if (n % 10 == 7) return 1;
		n /= 10;
	}
	
	return 0;
}

int pr() {
	int p = 1;
	bool kanan = 1;
	//cout << "i = 1, kotak = 1\n";
	FOR(i, 1, N - 1) {
		if (puter(i)) {
			kanan ^= 1;
		}
		
		if (kanan) {
			p++;
			if (p > N) p = 1;
		}
		else {
			p--;
			if (p < 1) p = N;
		}
		
		//printf("i = %d, kotak = %d\n", i + 1, p);
	} 
	
	return p;
}

int main () {
	cin >> T;
	
	while (T--) {
		cin >> N;
		cout << pr() << endl;
	}
}



