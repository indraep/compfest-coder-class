#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>


using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))

int T, N, D, K, temp;
int total[15010], in[15010];

bool cmp(int a, int b) {
	return a >= b;
}


int main () {
	cin >> T;
	
	while (T--) {
		cin >> N >> D >> K;
		
		total[N + 1] = 0;
		
		FOR(i, 1, N) {
			cin >> in[i];
		}
		
		FORD(i, N, 1) {
			temp = in[i];
			
			if (i + D > N) {
				total[i - 1] = total[i] + temp;
			}
			else {
				total[i - 1] = total[i] - in[i + D] + temp;
			}
		}
		
		sort(total, total + N, cmp);
		
		LL ans = 0;
		int caught = 0;
		
		REP(i, N) {
			if (total[i] > 0) {
				ans += (LL)(total[i]);
				caught++;
			}
			
			if (caught >= K) break;
		}
		
		cout << ans << endl;
	}
}
