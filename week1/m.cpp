#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>

using namespace std;

int pr(int N) {
	if (N < 4) return -1;
	N -= 4;
	
	int n;
	n = (-1.00 + sqrt(1.00 + 8.00 * N)) / 2.00;
	
	return n;
}

int tc, n;

int main () {
	cin >> tc;
	while(tc--) {
		cin >> n;
		cout << pr(n) << endl;
	}
}
