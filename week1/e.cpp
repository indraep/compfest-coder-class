#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>
#include <map>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010

int len, p = 0, a, b;
char * pola = "COMPFEST";
char c;

int main () {
	len = strlen(pola);
	a = b = 0;
	
	while (scanf("%c", &c) != EOF) {
		if (c >= 'a' && c <= 'z') {
			p = 0;
			a++;
		}
		else if (c >= 'A' && c <= 'Z') {
			b++;
			if (pola[p] == c) {
				p++;
				if (p == len) {
					printf("%d %d\n", a, b - len);
					a = 0;
					b = 0;
					p = 0;
				}
			}
			else {
				p = pola[0] == c ? 1 : 0;
			}
		}
	}
}





