#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>
#include <map>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010

int t, len, ganjil, genap;
char in[50];
int cnt[50];
vi temp;

LL fact(int a) {
	LL ans = 1;
	
	FOR(i, 2, a)
		ans *= (LL)(i);
		
	return ans;
}

int main () {
	
	cin >> t;
	
	while (t--) {
		CLEAR(cnt, 0);
		genap = ganjil = 0;
		
		cin >> in;
		
		len = strlen(in);
		REP(i, len) {
			cnt[in[i] - 'a']++;
		}
		
		temp.clear();
		REP(i, 26) {
			ganjil += cnt[i] % 2;
			if (cnt[i] > 1) {
				temp.pb(cnt[i] / 2);
				genap += cnt[i] / 2;
			}
		}
		
		LL ans = fact(genap);
		REP(i, temp.size()) {
			ans /= fact(temp[i]);
		}
		
		if (ganjil <= 1)
			cout << ans << endl;
		else
			cout << 0 << endl;
	}
}







