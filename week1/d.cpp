#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>
#include <map>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010


int t, in[3];
bool used[105][105][105];

bool valid(int a, int b, int c) {
	int temp[3];
	temp[0] = a; temp[1] = b; temp[2] = c;
	sort(temp, temp + 3);
	
	REP(i, 3)
		if (temp[i] > in[i]) return 0;
		
	return 1;
}

int brute() {
	int ans = 0;
	CLEAR(used, 0);
	FOR(i, 1, in[0]) {
		FOR(j, 1, in[1]) {
			FOR(k, 1, in[2]) {
				if (!used[i][j][k]) {
					used[i][j][k] = 1;
					ans++;
				}
				
				if (!used[i][k][j]) {
					used[i][k][j] = 1;
					ans++;
				}
				
				if (!used[j][i][k]) {
					used[j][i][k] = 1;
					ans++;
				}
				
				if (!used[j][k][i]) {
					used[j][k][i] = 1;
					ans++;
				}
				
				if (!used[k][i][j]) {
					used[k][i][j] = 1;
					ans++;
				}
				
				if (!used[k][j][i]) {
					used[k][j][i] = 1;
					ans++;
				}
			}
		}
	}
	
	return ans;
}


int f(int a, int b, int c) {
	return a * b * c;
}

int solve() {
	int a = in[0], b = in[1], c = in[2];
	
	int p = f(a, a, a);
	int q = (f(a, a, b) - f(a, a, a)) * 3;
	int r = (f(a, b, b))   - (p + q);
	//int s = (f(a, a, c) - f(a, a, b)) * 3;
	//int t = (f(a, b, c) - f(a, b, b)) * 6;
	
	return p + q + r;
}

int main () {
	cin >> t;
	
	while(t--) {
		REP(i, 3) cin >> in[i];
		
		sort(in, in + 3);
		
		printf("%d\n", brute());
	}
}





