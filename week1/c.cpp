#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>
#include <map>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010

int tc, t;
bool nyala;
char c;

int main () {
	scanf("%d\n", &tc);
	
	while (tc--) {
		nyala = 0;
		
		scanf("%d\n", &t);
		
		while (scanf("%c", &c), c != '\n') {
			if (c == 'N') nyala = 1;
		}
		
		puts(nyala ? "tidak mati lampu" : "mungkin mati lampu");
	}
}




