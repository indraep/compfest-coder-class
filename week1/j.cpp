#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <vector>
#include <queue>

using namespace std;

typedef pair < int , int > pii;
typedef vector < int > vi;
typedef vector < pii > vii;
typedef long long LL;

#define REP(i, a) for (int i = 0; i < (int)(a); i++)
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); i++)
#define REPD(i, a) for (int i = (int)(a - 1); i >= 0; i--)
#define FORD(i, a, b) for (int i = (int)(a); i >= (int)(b); i--)
#define mp make_pair
#define pb push_back
#define F first
#define S second
#define CLEAR(x, val) memset(x, val, sizeof(x))
#define MAX_N 1000010

int T, N, x, y;
pii kor[1010];

LL min(LL a, LL b) {
	if (a <= b) return a;
	return b;
}

LL max(LL a, LL b) {
	if (a >= b) return a;
	return b;
}

LL luas(int skip) {
	LL x1 = MAX_N, x2 = -MAX_N, y1 = MAX_N, y2 = -MAX_N;
	
	REP(i, N) {
		if (i == skip) continue;
		
		x1 = min(x1, kor[i].F);
		x2 = max(x2, kor[i].F);
		y1 = min(y1, kor[i].S);
		y2 = max(y2, kor[i].S);
 	}
 	
 	return (x2 - x1) * (y2 - y1);
}

int main () {
	cin >> T;
	while (T--) {
		cin >> N;
		
		REP(i, N) {
			scanf("%d %d", &x, &y);
			kor[i] = mp(x, y);
		}
		
		LL temp, ans = luas(0);
		
		FOR(i, 1, N - 1) {
			temp = luas(i);
			ans = min(ans, temp);
		}
		
		cout << ans << endl;
	}
}



